<?php

require_once "../crud/CrudTareas.php";
$obj = new CrudTareas();
$datos = $obj->MostrarDatos();
// encabezado tabla
$tabla = '	<table class="table table-dark">
                    <thead>
                        <tr class="font-weight-bold">
                            <td>Id</td>
                            <td>Fecha Inicio</td>
                            <td>Descripcion</td>
                            <td>Duracion(días)</td>
                            <td>Observaciones</td>
                            <td>Asignada a </td>
                        
                        </tr>
                    </thead>
                    <tbody>';
$datosTabla="";
foreach ($datos as $key =>$value){
    $datosTabla=$datosTabla.'<tr>
    <td>'.$value['idtarea'].'</td>
    <td>'.$value['fechaInicio'].'</td>
    <td>'.$value['descripcion'].'</td>
    <td>'.$value['duracion'].'</td>
    <td>'.$value['observaciones'].'</td>
    <td>'.$value['nombre'].'</td>
    
    <td>
        <span class="btn btn-warning btn-sm" onclick="obtenerDatos('.$value['idtarea'].')" data-toggle="modal" data-target="#actualizarModal">
            <i class="fas fa-edit"></i>
        </span>
        
    </td>
    <td>
        <span class="btn btn-danger btn-sm" onclick="eliminarDatos('.$value['idtarea'].')">
            <li class="fas fa-trash-alt"></li>
        </span>
    </td>
</tr>';



}

echo $tabla.$datosTabla.'</tbody></table>'; /**concatena cabecera datos y cieerre de la tabla  */








?>